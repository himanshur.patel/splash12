package com.example.coroutineflowlivedataexample.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainViewModel : ViewModel() {
    private val TAG = "MainViewModel"

    //LiveData
    private val _mainData: MutableLiveData<String> = MutableLiveData("HelloWorld")
    val mainData: LiveData<String> = _mainData

    //Flow Data
    lateinit var flow: Flow<Int>

    fun updateData() =
        viewModelScope.launch {
            _mainData.postValue("HelloTeam")
        }


    fun flowData() {
        flow = flow {
            (0..10).forEach {
                kotlinx.coroutines.delay(100)
                Log.d(TAG, "flowData: Emitting $it")
                emit(it)
            }
        }.map { it * it }.flowOn(Dispatchers.Main)

    }
}