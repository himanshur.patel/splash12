package com.example.coroutineflowlivedataexample.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.coroutineflowlivedataexample.R
import com.example.coroutineflowlivedataexample.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"
    private val mainViewModel: MainViewModel by viewModels()
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpObserver()
        binding.buttonOk.setOnClickListener {
            //Call livedata update
            mainViewModel.updateData()
        }
        binding.buttonFlowOk.setOnClickListener {
            //call flow live data
            CoroutineScope(Dispatchers.Main).launch {
                mainViewModel.flowData()
                mainViewModel.flow.collect {
                    Log.d(TAG, "onCreate: flow data $it")
                    binding.textViewMain.text = it.toString()
                }
            }
        }
    }

    private fun setUpObserver() {
        mainViewModel.mainData.observe(this) {
            binding.textViewMain.text = it
        }

    }
}