package com.example.coroutineflowlivedataexample.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen

class StartActivity : AppCompatActivity() {
    var isUserLoggedIn = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val splashActivity = installSplashScreen()
        splashActivity.setKeepOnScreenCondition { true }
        if (isUserLoggedIn) {
            startActivity(Intent(StartActivity@ this, MainActivity::class.java))
            finish()
        } else {
            //call login activity
        }
    }
}